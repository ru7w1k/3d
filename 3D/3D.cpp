// Headers
#include <Windows.h>
#include <stdio.h>
#include <math.h>
#include "3D.h"

// For PlaySound API
#pragma comment (lib, "Winmm.lib")

// Prototypes
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyDlgProc(HWND, UINT, WPARAM, LPARAM);
void Rotate(LPSTATE, UINT);
DWORD ThreadMove(LPVOID);
DWORD ThreadSpecial(LPVOID);
void ResetCamera(LPSTATE);
void ToggleFullScreen(LPSTATE);

//bad
char szLetters[100];
int iForShape;
char str[100];
//

// For Fire
DWORD WINAPI MyThreadProcOne(LPVOID);
static int R = 250;
static int G, B = 0;
static int iFire = 1;
static int x = 700, y = 130;
static int i = 1;
static int r = 0;

// External Functions
typedef void(*lpfnDrawShape)    (LPSTATE);
typedef void(*lpfnSetLetters)   (LPSTATE, char *);
typedef BOOL(*lpfnGenericShape) (LPSTATE, int, int*, int, int*);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variables
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("MyClass");

	// code
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH) /*CreateSolidBrush(RGB(240, 240, 240))*/;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register Class
	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szClassName,
		TEXT("3D"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd);

	// Message Loop
	while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

	}
	return msg.wParam;

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Shapes DLL variables 
	static HMODULE hLib = NULL;
	static HMODULE hLib2 = NULL;
	static lpfnDrawShape DrawShape = NULL;
	static lpfnSetLetters SetLetters = NULL;
	static lpfnGenericShape GenericShape = NULL;

	// WndProc variables 
	static STATE State = { 0 };
	static BOOL fSetOrigin = TRUE;
	static BOOL fShowHelp = FALSE;
	static BOOL fShowDebugInfo = FALSE;
	int iResult;


	// Dialog Box Variables
	// variables for Welcome message and dlg box
	static int iIsSpacePressed = 0;
	HFONT hFont;
	static HBRUSH hBrush = NULL;
	static HANDLE hThread1 = NULL;
	// ............  end

	RECT rc;
	PAINTSTRUCT ps;
	HDC hdc;
	POINT arrPoints[50];
	HPEN hPen;
	TCHAR lpszDebugInfo[512];
	static HANDLE hThreadMove;
	
	//char szLetters[] = "AstroMediComp";
	//char szLetters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//int arrPoints1[24] = { 0,5,-4,10,5,-4,0,0,-4,10,0,-4,0,5,4,10,5,4,0,0,4,10,0,4 };
	//int arrLines1[24] = { 0,1,1,2,2,3,3,0,4,5,5,6,6,7,7,4,0,4,1,5,2,6,3,7 };

	switch (iMsg)
	{

#pragma region WM_CREATE
	case WM_CREATE:

		// start main theme music
		//PlaySound(MAKEINTRESOURCE(MAINTHEME), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_LOOP );

		// Load DLLs
		hLib = LoadLibrary(TEXT("Shapes.dll"));
		if (!hLib)
		{
			MessageBox(hwnd, TEXT("Cannot Load Shapes.dll..."), TEXT("Error"), MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}

		// Initialize the State
		State.hwnd = hwnd;
		State.Camera = { 0, 0, 0, 40 };
		State.iAnimate = 0;
		State.Directions[0] = 1;
		State.Directions[1] = 1;
		State.Directions[2] = 1;
		State.Offset[0] = 0;
		State.Offset[1] = 0;
		State.Origin[0] = 0;
		State.Origin[1] = 0;
		State.fAxis = FALSE;
		State.Model.iNoOfPoints = 0;
		State.Model.iNoOfLines = 0;
		State.iNoOfLetters = 0;

		memset(State.arrOffsets, 0, sizeof(int) * 250);

		//Cube(&State);
		//ThreeCubes(&State);
		//FiveAngleStar(&State);

		// DIRTY IMPLIMENTATION
		hLib2 = LoadLibrary(TEXT("AlphaNumeric.dll"));
		if (!hLib2)
		{
			MessageBox(hwnd, TEXT("Cannot Load AlphaNumeric.dll..."), TEXT("Error"), MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}
		SetLetters = (lpfnSetLetters)GetProcAddress(hLib2, "SetLetters");
		GenericShape = (lpfnGenericShape)GetProcAddress(hLib, "GenericShape");

		//sprintf_s(szLetters, "This\nIs\nMultiline\nString");
		/*sprintf_s(szLetters, "Those That Know\ndo\n\nThose That Understand\nTeach");
		SetLetters(&State, szLetters);
		iIsSpacePressed = 2;*/
		//GenericShape(&State, 8, (int *)arrPoints1, 12, (int *)arrLines1);

		// DIRTY

		/// FIRE

		hThread1 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyThreadProcOne, (LPVOID)hwnd, 0, NULL);
		if (hThread1 == NULL)
		{
			MessageBox(hwnd, TEXT("Thread 1 creation failed "), TEXT("ERROR MSG"), MB_OK);
			DestroyWindow(hwnd);
		}

		/// FIRE ///


		break;
#pragma endregion

#pragma region WM_PAINT
	case WM_PAINT:

		// Begin!
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		
		// on Main Dialog Box 
		if (iIsSpacePressed == 0)
		{
			ResumeThread(hThread1);

			//iIsSpacePressed = 1;
			SetBkMode(hdc, TRANSPARENT);
			SetBkColor(hdc, RGB(0, 0, 0));
			hFont = CreateFont(30, 0, 0, 0, FW_SEMIBOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, FF_DONTCARE, TEXT("Consolas"));

			SelectObject(hdc, hFont);
			SetTextColor(hdc, RGB(255, 255, 255));
			DrawText(hdc, TEXT("\n\n\n        PRESS   FOR SHAPES"), -1, &rc, DT_LEFT | DT_VCENTER);
			DrawText(hdc, TEXT("\n\n\n\n\n            PRESS   FOR LETTERS"), -1, &rc, DT_LEFT | DT_VCENTER);
			DrawText(hdc, TEXT("\n\n\n\n\n\n\n        PRESS   FOR HELP"), -1, &rc, DT_LEFT | DT_VCENTER);
			DrawText(hdc, TEXT("\n\n\n\n\n\n\n\n\n            PRESS     TO QUIT"), -1, &rc, DT_LEFT | DT_VCENTER);

			SetTextColor(hdc, RGB(255, 0, 0));
			DrawText(hdc, TEXT("\n\n\n              1           "), -1, &rc, DT_LEFT | DT_VCENTER);
			DrawText(hdc, TEXT("\n\n\n\n\n                  2            "), -1, &rc, DT_LEFT | DT_VCENTER);
			DrawText(hdc, TEXT("\n\n\n\n\n\n\n              H            "), -1, &rc, DT_LEFT | DT_VCENTER);
			DrawText(hdc, TEXT("\n\n\n\n\n\n\n\n\n                  ESC        "), -1, &rc, DT_LEFT | DT_VCENTER);

			DeleteObject(hFont);
			/// FIRE 
			hBrush = CreateSolidBrush(RGB(R, G, B));
			hPen = CreatePen(PS_SOLID, 1, RGB(R, G, B));
			SelectObject(hdc, hBrush);
			SelectObject(hdc, hPen);

			switch (iFire)
			{

			case 1:
				Chord(hdc, 100 * i + x, 50 * i + y + r, 250 * i + x, 400 * i + y, 140 * i + x, 50 * i + y, 230 * i + x, 370 * i + y);				// main back l
				Chord(hdc, 150 * i + x, 50 * i + y + r, 330 * i + x, 350 * i + y, 200 * i + x, 50 * i + y, 230 * i + x, 370 * i + y);				// main back r
				Chord(hdc, 50 * i + x, 100 * i + y + r, 200 * i + x, 350 * i + y, 70 * i + x, 90 * i + y, 200 * i + x, 300 * i + y);				// front left
				Chord(hdc, 140 * i + x, 100 * i + y + r, 290 * i + x, 350 * i + y, 180 * i + x, 250 * i + y, 240 * i + x, 100 * i + y);			// front right
				Chord(hdc, 20 * i + x, 130 * i + y + r, 170 * i + x, 380 * i + y, 20 * i + x, 150 * i + y, 150 * i + x, 370 * i + y);				// front front left
				Chord(hdc, 180 * i + x, 150 * i + y + r, 330 * i + x, 400 * i + y, 220 * i + x, 300 * i + y, 320 * i + x, 50 * i + y);				// front front right
				break;

			case 2:
				Chord(hdc, 100 * i + x, 30 * i + y - r, 250 * i + x, 400 * i + y, 140 * i + x, 50 * i + y, 230 * i + x, 370 * i + y);				// main back l 
				Chord(hdc, 150 * i + x, 30 * i + y - r, 330 * i + x, 250 * i + y, 200 * i + x, 50 * i + y, 230 * i + x, 370 * i + y);				// main back r
				Chord(hdc, 50 * i + x, 100 * i + y - r, 200 * i + x, 350 * i + y, 80 * i + x, 80 * i + y, 200 * i + x, 300 * i + y);				// front left
				Chord(hdc, 140 * i + x, 70 * i + y - r, 270 * i + x, 350 * i + y, 180 * i + x, 250 * i + y, 240 * i + x, 100 * i + y);				// front right
				Chord(hdc, 20 * i + x, 130 * i + y - r, 170 * i + x, 380 * i + y, 20 * i + x, 150 * i + y, 150 * i + x, 370 * i + y);				// front front left
				Chord(hdc, 180 * i + x, 150 * i + y - r, 330 * i + x, 400 * i + y, 220 * i + x, 300 * i + y, 320 * i + x, 50 * i + y);
				break;
			}

			Chord(hdc, 20 * i + x, 50 * i + y, 330 * i + x, 450 * i + y, 20 * i + x, 300 * i + y, 330 * i + x, 300 * i + y);

			/// FIRE ///

		}

		else if (iIsSpacePressed == 1 )
		{
			SuspendThread(hThread1);
		}

		// On Rendering Screen
		else
		{
			SuspendThread(hThread1);
			SetBkColor(hdc, RGB(0, 0, 0));
			// Create White Pen
			hPen = CreatePen(PS_DASH, 1, RGB(0, 255, 0));
			SelectObject(hdc, hPen);

			// Find Origin if flag is set
			if (fSetOrigin)
			{
				State.Origin[0] = (rc.right / 2) + State.Offset[0];
				State.Origin[1] = (rc.bottom / 2) + State.Offset[1];
				fSetOrigin = FALSE;
			}

			if (State.fAxis) {
				// x-axis
				arrPoints[0].x = 0;
				arrPoints[0].y = (rc.bottom / 2) + State.Offset[1];
				arrPoints[1].x = rc.right;
				arrPoints[1].y = (rc.bottom / 2) + State.Offset[1];
				Polyline(hdc, arrPoints, 2);

				// y-axis
				arrPoints[0].x = (rc.right / 2) + State.Offset[0];
				arrPoints[0].y = 0;
				arrPoints[1].x = (rc.right / 2) + State.Offset[0];
				arrPoints[1].y = rc.bottom;
				Polyline(hdc, arrPoints, 2);
			}

			// Create White Pen
			hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
			SelectObject(hdc, hPen);

			//// z-axis
			//arrPoints[0].x = (rc.right / 2) + Offset[0];
			//arrPoints[0].y = 0;
			//arrPoints[1].x = (rc.right / 2) + Offset[0];
			//arrPoints[1].y = rc.bottom;
			//Polyline(hdc, arrPoints, 2);

			// Draw Object
			for (int i = 0; i < State.Model.iNoOfLines; i++)
			{
				arrPoints[0].x = (State.Camera.iScale * State.Model.arrLine[i].ptStart->x) + State.Origin[0];
				arrPoints[0].y = -(State.Camera.iScale * State.Model.arrLine[i].ptStart->y) + State.Origin[1];
				arrPoints[1].x = (State.Camera.iScale * State.Model.arrLine[i].ptEnd->x) + State.Origin[0];
				arrPoints[1].y = -(State.Camera.iScale * State.Model.arrLine[i].ptEnd->y) + State.Origin[1];
				Polyline(hdc, arrPoints, 2);
			}

			hFont = CreateFont(20, 0, 0, 0, FW_SEMIBOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, FF_DONTCARE, TEXT("Consolas"));
			SelectObject(hdc, hFont);

			// Draw Debug Info
			if (fShowDebugInfo)
			{
				SetBkMode(hdc, TRANSPARENT);
				SetTextColor(hdc, RGB(255, 255, 255));
				sprintf_s(lpszDebugInfo, "Angles:\n\tX: %d\n\tY: %d\n\tZ: %d\n\nDirections:\n\tX: %d\n\tY: %d\n\tZ: %d\n\nScale: %d\n\nOrigin:\n\tX: %d\n\tY: %d",
					State.Camera.xAngle, State.Camera.yAngle, State.Camera.zAngle,
					State.Directions[0], State.Directions[1], State.Directions[2],
					State.Camera.iScale,
					State.Origin[0], State.Origin[1]);
				DrawText(hdc, lpszDebugInfo, -1, &rc, DT_TOP | DT_LEFT);
			}

			// Draw Help Info
			if (fShowHelp)
			{
				SetBkMode(hdc, TRANSPARENT);
				SetTextColor(hdc, RGB(255, 255, 255));
				DrawText(hdc, TEXT("\n Use  ,  ,  ,   To Move Object"), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n Use           ,             for X - Axis rotation"), -1, &rc, DT_TOP | DT_LEFT);		 
				DrawText(hdc, TEXT("\n\n\n Use         ,            for Y - Axis rotation"), -1, &rc, DT_TOP | DT_LEFT);		 
				DrawText(hdc, TEXT("\n\n\n\n Use  ,   for Z - Axis rotation"), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n Use   for Help"), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n\n Use   for Debug Info"), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n\n\n Use   For Shape"), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n\n\n\n Use   For Letters"), -1, &rc, DT_TOP | DT_LEFT);

				SetTextColor(hdc, RGB(255, 0, 0));

				DrawText(hdc, TEXT("\n     W  A  S  D               "), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n     Left Arrow, Right Arrow                      "), -1, &rc, DT_TOP | DT_LEFT);		 
				DrawText(hdc, TEXT("\n\n\n     Up Arrow, Down Arrow                      "), -1, &rc, DT_TOP | DT_LEFT);		 
				DrawText(hdc, TEXT("\n\n\n\n     Z, X                      "), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n     H         "), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n\n     E               "), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n\n\n     1          "), -1, &rc, DT_TOP | DT_LEFT);
				DrawText(hdc, TEXT("\n\n\n\n\n\n\n\n     2            "), -1, &rc, DT_TOP | DT_LEFT);

			}
		}

		EndPaint(hwnd, &ps);
		break;
#pragma endregion

#pragma region WM_KEYDOWN
	case WM_KEYDOWN:

		switch (wParam)
		{
			// case for shape and alphanumeric dlgbox
		case '1':
			iIsSpacePressed = 1;
			GetClientRect(hwnd, &rc);
			InvalidateRect(hwnd, &rc, TRUE);

			State.iNoOfLetters = 0;
			State.Camera.xAngle = 0;
			State.Camera.yAngle = 0;
			State.Camera.zAngle = 0;

			if (DialogBox((HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE), MAKEINTRESOURCE(DATAENTRY), hwnd, (DLGPROC)MyDlgProc) == IDSUBMIT1)
			{
				//wsprintf(str, "%d", iForShape);

				//MessageBox(hwnd, str, TEXT("MSG"), MB_OK);
				switch (iForShape)
				{
				case 1:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Cube");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found Cube function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;

				case 2:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "ThreeCubes");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found ThreeCube function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;

				case 3:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Pyramid");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found Pyramid function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;

				case 4:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Diamond");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found Diamond function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;

				case 5:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "FiveAngleStar");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found FiveAngleStar function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;
				
				case 6:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Football");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found Football function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;

				case 7:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Tetrahedron");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found Tetrahedron function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;

				case 8:
					DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Octahedron");
					if (!DrawShape)
					{
						MessageBox(hwnd, TEXT("Cannot found Tetrahedron function"), TEXT("Error"), MB_OK | MB_ICONERROR);
					}
					DrawShape(&State);
					break;

				}
				iIsSpacePressed = 2;
			}
			else
			{
				iIsSpacePressed = 0;
			}
			InvalidateRect(hwnd, NULL, FALSE);
			break;


		case '2':
			iIsSpacePressed = 1;
			GetClientRect(hwnd, &rc);
			InvalidateRect(hwnd, &rc, TRUE);
			if (DialogBox((HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE), MAKEINTRESOURCE(DATAENTRY1), hwnd, (DLGPROC)MyDlgProc) == IDSUBMIT)
			{
				State.Offset[0] = State.Offset[1] = 0;
				State.Camera.iScale = 40;

				State.Model.iNoOfLines = 0;
				State.Model.iNoOfPoints = 0;
				SetLetters(&State, szLetters);
				iIsSpacePressed = 2;
			}
			else
			{
				iIsSpacePressed = 0;
			}
			InvalidateRect(hwnd, NULL, FALSE);
			break;

			// case ends here

		case 'W':
			State.Origin[1] -= 5;
			break;

		case 'A':
			State.Origin[0] -= 5;
			break;

		case 'D':
			State.Origin[0] += 5;
			break;

		case 'S':
			State.Origin[1] += 5;
			break;

		/*case '1':
			DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Cube");
			if (!DrawShape)
			{
				MessageBox(hwnd, TEXT("Cannot found Cube function"), TEXT("Error"), MB_OK | MB_ICONERROR);
			}
			DrawShape(&State);
			break;

		case '2':
			DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "ThreeCubes");
			if (!DrawShape)
			{
				MessageBox(hwnd, TEXT("Cannot found ThreeCube function"), TEXT("Error"), MB_OK | MB_ICONERROR);
			}
			DrawShape(&State);
			break;

		case '3':
			DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Pyramid");
			if (!DrawShape)
			{
				MessageBox(hwnd, TEXT("Cannot found Pyramid function"), TEXT("Error"), MB_OK | MB_ICONERROR);
			}
			DrawShape(&State);
			(&State);
			break;

		case '4':
			DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Diamond");
			if (!DrawShape)
			{
				MessageBox(hwnd, TEXT("Cannot found Diamond function"), TEXT("Error"), MB_OK | MB_ICONERROR);
			}
			DrawShape(&State);
			break;

		case '5':DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "FiveAngleStar");
			if (!DrawShape)
			{
				MessageBox(hwnd, TEXT("Cannot found FiveAngleStar function"), TEXT("Error"), MB_OK | MB_ICONERROR);
			}
			DrawShape(&State);
			break;

		case '6':DrawShape = (lpfnDrawShape)GetProcAddress(hLib, "Football");
			if (!DrawShape)
			{
				MessageBox(hwnd, TEXT("Cannot found Football function"), TEXT("Error"), MB_OK | MB_ICONERROR);
			}
			DrawShape(&State);
			break;*/

		case 'P':
			
			iIsSpacePressed = 2;
			State.Camera.iScale = 7;
			sprintf_s(szLetters, "ASTROMEDICOMP");
			SetLetters(&State, szLetters);
			fSetOrigin = FALSE;
			State.Origin[0] = 683;
			State.Origin[1] = 492;
			CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadSpecial, (LPVOID)&State, 0, NULL);

			break;

		case 'I':
			State.Camera.iScale += 1;
			break;

		case 'O':
			if (State.Camera.iScale > 2)
				State.Camera.iScale -= 1;
			break;

		case VK_SPACE:
			if (State.iAnimate == 0) {
				State.iAnimate = 1;
				hThreadMove = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadMove, (LPVOID)&State, 0, NULL);
			}
			else
			{
				State.iAnimate = 0;
				SuspendThread(hThreadMove);
			}
			break;

		case 'R':
			State.Offset[0] = State.Offset[1] = 0;
			State.Camera.iScale = 40;

			if (State.iNoOfLetters != 0)
			{
				State.Model.iNoOfLines = 0;
				State.Model.iNoOfPoints = 0;
				SetLetters(&State, szLetters);
			}

			else {
				State.Directions[0] = -State.Camera.xAngle;
				State.Directions[1] = -State.Camera.yAngle;
				State.Directions[2] = -State.Camera.zAngle;

				Rotate(&State, XAXIS);
				Rotate(&State, YAXIS);
				Rotate(&State, ZAXIS);
			}
			ResetCamera(&State);

			break;

		case 'Q':
			if (State.fAxis)
				State.fAxis = FALSE;
			else
				State.fAxis = TRUE;

			break;

		// for Help
		case 'H':
			fShowDebugInfo = FALSE;

			if (fShowHelp)
				fShowHelp = FALSE;
			else
				fShowHelp = TRUE;

			break;

		// for Debug Info
		case 'E':
			if (State.iNoOfLetters != 0 || State.Model.iNoOfPoints == 0)
				break;
			
			fShowHelp = FALSE;

			if (fShowDebugInfo)
				fShowDebugInfo = FALSE;
			else
				fShowDebugInfo = TRUE;

			break;

		case VK_UP:
			// Rotate along X-Axis
			State.Directions[0] = 1;
			Rotate(&State, XAXIS);
			break;

		case VK_DOWN:
			// Rotate along X-Axis Negative
			State.Directions[0] = -1;
			Rotate(&State, XAXIS);
			break;

		case VK_LEFT:
			// Rotate along Y-Axis 
			State.Directions[1] = 1;
			Rotate(&State, YAXIS);
			break;

		case VK_RIGHT:
			// Rotate along Y-Axis Negative
			State.Directions[1] = -1;
			Rotate(&State, YAXIS);
			break;

		case 'Z':
			// Rotate along Z-Axis
			State.Directions[2] = 1;
			Rotate(&State, ZAXIS);
			break;

		case 'X':
			// Rotate along Z-Axis Negative
			State.Directions[2] = -1;
			Rotate(&State, ZAXIS);
			break;

		case 'F':
			// Fullscreen Toggle
			ToggleFullScreen(&State);
			break;

		case VK_ESCAPE:
			iResult = MessageBox(hwnd, TEXT("Do you want to Quit?"), TEXT("3D"), MB_YESNO | MB_ICONQUESTION);
			if (iResult == IDYES) DestroyWindow(hwnd);
			break;
		}

		GetClientRect(hwnd, &rc);
		InvalidateRect(hwnd, &rc, TRUE);
		break;

#pragma endregion

	case WM_DESTROY:
		FreeLibrary(hLib);
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

BOOL CALLBACK MyDlgProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static HBRUSH hBrush = NULL;
	static int lbItem;
	static HWND hwndList;
	switch (iMsg)
	{

	case WM_INITDIALOG:
	{
		HWND hwndList = GetDlgItem(hwnd, ID_LISTBOX);
		int pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"CUBE");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)1);

		pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"THREE CUBE");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)2);

		pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"PYRAMID");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)3);

		pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"DIMOND");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)4);

		pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"STARS");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)5);

		pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"FOOTBALL");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)6);

		pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"TETRAHEDRON");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)7);

		pos = SendMessage(hwndList, CB_ADDSTRING, 0, (LPARAM)"OCTAHEDRON");
		SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM)8); 

		SetFocus(hwndList);
		return TRUE;
	}

	case WM_CTLCOLORDLG:
	case WM_CTLCOLORBTN:
	case WM_CTLCOLOREDIT:
	case WM_CTLCOLORLISTBOX:
	case WM_CTLCOLORSTATIC:
	case WM_CTLCOLORMSGBOX:
		SetBkColor((HDC)wParam, RGB(0, 0, 0));
		//SetBkMode((HDC)wParam, TRANSPARENT);
		SetTextColor((HDC)wParam, RGB(255, 255, 255));
		hBrush = CreateSolidBrush(RGB(0, 0, 0));
		return (INT_PTR)hBrush;

		break;

	case WM_COMMAND:

		switch (HIWORD(wParam))
		{
		case CTLCOLOR_DLG:
			return TRUE;

		case  CBN_SELCHANGE:
			// get the selected index from the combo box list
			iForShape = (int)SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

			// shape numbers are indexed from 1, so increment iForShape
			iForShape++;

			//iForShape = (int)SendMessage(hwndList, CB_GETITEMDATA, lbItem, 0);
			return TRUE;
		}

		switch (LOWORD(wParam))
		{
			
		case IDSUBMIT1:
			hwndList = GetDlgItem(hwnd, ID_LISTBOX);
			//wsprintf(str, "%d", iForShape);
			//GetDlgItemText(hwnd, ID_LISTBOX, szShape, 100);
			//MessageBox(hwnd, str, TEXT("MSG"), MB_OK);
			EndDialog(hwnd, wParam);
			return TRUE;

		case IDSUBMIT:
			GetDlgItemText(hwnd, ID_ETALNUM, szLetters, 100);
			EndDialog(hwnd, wParam);
			return TRUE;

		case IDCANCEL:
			EndDialog(hwnd, wParam);
			return TRUE;
		}
		return TRUE;
	}
	return FALSE;
}

void Rotate(LPSTATE State, UINT iAxis)
{
	double dRotationMatrix[3][3];
	double dSin = 0;
	double dCos = 0;
	double dTmp[3];

	switch (iAxis)
	{

	case XAXIS:

		// Adjust Camera Angle
		if (State->Directions[0] == 1)
		{
			if (State->Camera.xAngle < 359)
				State->Camera.xAngle++;
			else State->Camera.xAngle = 0;
		}

		if (State->Directions[0] == -1)
		{
			if (State->Camera.xAngle > 0)
				State->Camera.xAngle--;
			else State->Camera.xAngle = 359;
		}

		// Calculate Rotation Matrix 
		dSin = sin(State->Directions[0] * PI / 180);
		dCos = cos(State->Directions[0] * PI / 180);

		dRotationMatrix[0][0] = 1;
		dRotationMatrix[0][1] = 0;
		dRotationMatrix[0][2] = 0;

		dRotationMatrix[1][0] = 0;
		dRotationMatrix[1][1] = dCos;
		dRotationMatrix[1][2] = -dSin;

		dRotationMatrix[2][0] = 0;
		dRotationMatrix[2][1] = dSin;
		dRotationMatrix[2][2] = dCos;

		break;

	case YAXIS:

		// Adjust Camera Angle
		if (State->Directions[1] == 1)
		{
			if (State->Camera.yAngle < 359)
				State->Camera.yAngle++;
			else State->Camera.yAngle = 0;
		}

		if (State->Directions[1] == -1)
		{
			if (State->Camera.yAngle > 0)
				State->Camera.yAngle--;
			else State->Camera.yAngle = 359;
		}

		// Calculate Rotation Matrix 
		dSin = sin(State->Directions[1] * PI / 180);
		dCos = cos(State->Directions[1] * PI / 180);

		dRotationMatrix[0][0] = dCos;
		dRotationMatrix[0][1] = 0;
		dRotationMatrix[0][2] = dSin;

		dRotationMatrix[1][0] = 0;
		dRotationMatrix[1][1] = 1;
		dRotationMatrix[1][2] = 0;

		dRotationMatrix[2][0] = -dSin;
		dRotationMatrix[2][1] = 0;
		dRotationMatrix[2][2] = dCos;

		break;

	case ZAXIS:

		// Adjust Camera Angle
		if (State->Directions[2] == 1)
		{
			if (State->Camera.zAngle < 359)
				State->Camera.zAngle++;
			else State->Camera.zAngle = 0;
		}

		if (State->Directions[2] == -1)
		{
			if (State->Camera.zAngle > 0)
				State->Camera.zAngle--;
			else State->Camera.zAngle = 359;
		}

		// Calculate Rotation Matrix 
		dSin = sin(State->Directions[2] * PI / 180);
		dCos = cos(State->Directions[2] * PI / 180);

		dRotationMatrix[0][0] = dCos;
		dRotationMatrix[0][1] = -dSin;
		dRotationMatrix[0][2] = 0;

		dRotationMatrix[1][0] = dSin;
		dRotationMatrix[1][1] = dCos;
		dRotationMatrix[1][2] = 0;

		dRotationMatrix[2][0] = 0;
		dRotationMatrix[2][1] = 0;
		dRotationMatrix[2][2] = 1;

		break;

	}

	for (int i = 0; i < State->Model.iNoOfPoints; i++)
	{
		dTmp[0] = (dRotationMatrix[0][0] * State->Points[i].x) + (dRotationMatrix[0][1] * State->Points[i].y) + (dRotationMatrix[0][2] * State->Points[i].z);
		dTmp[1] = (dRotationMatrix[1][0] * State->Points[i].x) + (dRotationMatrix[1][1] * State->Points[i].y) + (dRotationMatrix[1][2] * State->Points[i].z);
		dTmp[2] = (dRotationMatrix[2][0] * State->Points[i].x) + (dRotationMatrix[2][1] * State->Points[i].y) + (dRotationMatrix[2][2] * State->Points[i].z);

		State->Points[i].x = dTmp[0];
		State->Points[i].y = dTmp[1];
		State->Points[i].z = dTmp[2];
	}

}

void LettersEffect(LPSTATE lpState)
{
	RECT rc;
	double dTmp[3];

	// Calculate Rotation Matrix
	// Rotation along X-Axis with 10 Degree
	double dRotationMatrix[3][3] = { 0 };
	dRotationMatrix[0][0] = 1;
	dRotationMatrix[0][1] = 0;
	dRotationMatrix[0][2] = 0;

	dRotationMatrix[1][0] = 0;
	dRotationMatrix[1][1] = 0.98480775301;
	dRotationMatrix[1][2] = 0.17364817766;

	dRotationMatrix[2][0] = 0;
	dRotationMatrix[2][1] = -0.17364817766;
	dRotationMatrix[2][2] = 0.98480775301;

	// set state to begin rotation!
	int i = 0;
	int iBegin = 249;
	int iEnd = 0;

	for (int k = 0; k < 36 + lpState->iNoOfLetters; k++)
	{
		for (int j = lpState->arrOffsets[iBegin]; j < lpState->arrOffsets[iEnd]; j++)
		{
			dTmp[0] = (dRotationMatrix[0][0] * lpState->Points[j].x) + (dRotationMatrix[0][1] * lpState->Points[j].y) + (dRotationMatrix[0][2] * lpState->Points[j].z);
			dTmp[1] = (dRotationMatrix[1][0] * lpState->Points[j].x) + (dRotationMatrix[1][1] * lpState->Points[j].y) + (dRotationMatrix[1][2] * lpState->Points[j].z);
			dTmp[2] = (dRotationMatrix[2][0] * lpState->Points[j].x) + (dRotationMatrix[2][1] * lpState->Points[j].y) + (dRotationMatrix[2][2] * lpState->Points[j].z);

			lpState->Points[j].x = dTmp[0];
			lpState->Points[j].y = dTmp[1];
			lpState->Points[j].z = dTmp[2];
		}

		GetClientRect(lpState->hwnd, &rc);
		InvalidateRect(lpState->hwnd, &rc, TRUE);
		Sleep(80);

		if (k < lpState->iNoOfLetters - 1)
		{
			iEnd++;
		}

		if (k == 35)
		{
			iBegin = 0;
		}

		if (k > 35)
		{
			iBegin++;
		}

	}

	// reset state to rotate again!
	i = 0;
	iBegin = 249;
	iEnd = 0;

	lpState->iAnimate = 0;
}

DWORD ThreadMove(LPVOID State)
{
	LPSTATE lpState = (LPSTATE)State;
	LettersEffect(lpState);
	return 0;
}

DWORD ThreadSpecial(LPVOID State)
{
	LPSTATE lpState = (LPSTATE)State;
	
	PlaySound(NULL, GetModuleHandle(NULL), 0);
	PlaySound(MAKEINTRESOURCE(CHANAKYA), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_LOOP);
	
	for (int i = 0; i < 5; i++)
	{
		LettersEffect(lpState);
		Sleep(4000);
	}

	PlaySound(NULL, GetModuleHandle(NULL), 0);
	PlaySound(MAKEINTRESOURCE(MAINTHEME), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_LOOP);

	return 0;
}

void ResetCamera(LPSTATE State)
{
	State->Directions[0] = 1;
	State->Directions[1] = 1;
	State->Directions[2] = 1;

	State->Camera.xAngle = 0;
	State->Camera.yAngle = 0;
	State->Camera.zAngle = 0;
}

// For Fire
DWORD WINAPI MyThreadProcOne(LPVOID param)
{
	HWND hwnd = HWND(param);
	while (1)
	{
	if (R == 250)
	{
		R = 170;
	}
	else
	{
		R = R + 1;
	}
	if (G == 50)
	{
		G = 0;
	}
	else
	{
		G = G + 1;
	}
	//iFire = rand() % 2;
	if (iFire == 1)
		iFire = 2;
	else
		iFire = 1;
	r = rand() % 50;
		InvalidateRect(hwnd, NULL, TRUE);
		Sleep(150);
	}
	return 0;
}

void ToggleFullScreen(LPSTATE State)
{
	static bool bIsFullScreen = false;
	static DWORD dwStyle;
	static WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
	MONITORINFO MI;

	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(State->hwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			MI = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(State->hwnd, &wpPrev)
				&& GetMonitorInfo(MonitorFromWindow(State->hwnd, MONITORINFOF_PRIMARY), &MI))
			{
				SetWindowLong(State->hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(State->hwnd,
					HWND_TOP,
					MI.rcMonitor.left,
					MI.rcMonitor.top,
					MI.rcMonitor.right - MI.rcMonitor.left,
					MI.rcMonitor.bottom - MI.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else
	{
		SetWindowLong(State->hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(State->hwnd, &wpPrev);
		SetWindowPos(State->hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		//ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}
